# create Tk window and PhotoImage object
from tkinter import Tk, Canvas, PhotoImage, mainloop
# store color of each pixel of fractal image
import sys
import GradientFactory, FractalFactory
# color is chosen from Gradient
# particular color from array of possibilities is determined from Mandelbrot or Julia


class ImagePainter:
    def paint(self, config, gradient=''):
        fractal = FractalFactory.makeFractal(config)
        gradientColors = GradientFactory.makeGradient(gradient.lower(), config['iterations']).getGradientColorMap()
        window = Tk()
        img = PhotoImage(width=config['pixels'], height=config['pixels'])
        portion = int(config['pixels'] / 64)
        for col in range(config['pixels']):
            if col % portion == 0:
                # Update the status bar each time we complete 1/64th of the rows
                pips = col // portion
                pct = col / config['pixels']
                print(f"{config['imageName']} ({config['pixels']}x{config['pixels']}) {'=' * pips}{'_' * (64 - pips)} {pct:.0%}", end='\r', file=sys.stderr)
            for row in range(config['pixels']):
                fractal.setValueC(complex(config['min']['x'] + col * config['pixelsize'], config['min']['y'] + row * config['pixelsize']))
                img.put(gradientColors[fractal.count()], (col, row))
        print(f"{config['imageName']} ({config['pixels']}x{config['pixels']}) ================================================================ 100%", file=sys.stderr)

    # Display the image on the screen
        canvas = Canvas(window, width=config['pixels'], height=config['pixels'], bg=gradientColors[0])
        canvas.pack()
        canvas.create_image((int(config['pixels']/2), int(config['pixels']/2)), image=img, state="normal")
        img.write(config['imageName'] + ".png")
        print(f"Wrote image {config['imageName']}.png")
        mainloop()
