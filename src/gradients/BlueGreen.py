# given coordinate in complex plane
# return the iteration count of function at points
from Gradient import Gradient
from Color import Color


class BlueGreen(Gradient):
    def __init__(self, iterations):
        super().__init__(iterations)

    def getColorList(self):
        return [Color(42, 245, 152), Color(34, 228, 172), Color(27, 215, 187),
                Color(20, 201, 203), Color(15, 190, 216), Color(8, 179, 229)]
