# given coordinate in complex plane
# return the iteration count of function at points
from Gradient import Gradient
from Color import Color


class Royal(Gradient):
    def __init__(self, iterations):
        super().__init__(iterations)

    def getColorList(self):
        return [Color(10, 38, 98), Color(29, 130, 210), Color(255, 255, 255),
                Color(255, 220, 26), Color(160, 0, 0)]
