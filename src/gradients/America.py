# given coordinate in complex plane
# return the iteration count of function at points
from Gradient import Gradient
from Color import Color


class America(Gradient):
    def __init__(self, iterations):
        super().__init__(iterations)

    def getColorList(self):
        return [Color(200, 26, 28), Color(236, 236, 236), Color(128, 128, 128),
                Color(217, 217, 217), Color(35, 86, 170)]
