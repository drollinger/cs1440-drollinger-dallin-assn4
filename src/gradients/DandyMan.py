# given coordinate in complex plane
# return the iteration count of function at points
from Gradient import Gradient
from Color import Color


class DandyMan(Gradient):
    def __init__(self, iterations):
        super().__init__(iterations)

    def getColorList(self):
        return [Color(10, 10, 10), Color(180, 180, 181), Color(226, 226, 266),
                Color(0, 93, 201), Color(0, 0, 255), Color(0, 72, 157)]
