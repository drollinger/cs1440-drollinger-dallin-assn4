# given coordinate in complex plane
# return the iteration count of function at points
from Gradient import Gradient
from Color import Color


class Rainbow(Gradient):
    def __init__(self, iterations):
        super().__init__(iterations)

    def getColorList(self):
        return [Color(255, 0, 0), Color(255, 255, 0), Color(0, 255, 0),
                Color(0, 255, 255), Color(0, 0, 255), Color(255, 0, 255)]
