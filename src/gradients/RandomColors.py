# given coordinate in complex plane
# return the iteration count of function at points
from Gradient import Gradient
from random import randint
import Color


class RandomColors(Gradient):
    def __init__(self, iterations):
        super().__init__(iterations)

    def getColorList(self):
        numberOfColors = int(self.iterations / 10)
        if numberOfColors < 2:
            numberOfColors = 2
        generatedList = []
        for i in range(numberOfColors):
            generatedList.extend([Color.Color(randint(0, 255), randint(0, 255), randint(0, 255))])
        return generatedList
