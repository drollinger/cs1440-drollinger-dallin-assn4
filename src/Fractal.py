from abc import ABC

class Fractal(ABC):

    def __init__(self, iterations, c, z=complex(0, 0), divertingCutOff=2):
        self.iterations = iterations
        self.z = z
        self.c = c
        self.divertingCutOff = divertingCutOff

    def count(self):
        zStep = self.z
        for i in range(self.iterations):
            zStep = self.iterationType(zStep)
            if abs(zStep) > self.divertingCutOff:
                return i
        return self.iterations - 1

    def iterationType(self, zStep):
        pass

    def setValueC(self, newValue):
        self.c = newValue
