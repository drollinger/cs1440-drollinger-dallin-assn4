# import everything important
import sys
import Config, ImagePainter

# use user input
if len(sys.argv) >= 2:
    # call functions from modules to create configuration file and create image
    config = Config.Config(sys.argv[1]).configDict
    ImagePainter.ImagePainter().paint(config) if len(sys.argv) == 2 else ImagePainter.ImagePainter().paint(config, sys.argv[2])
else:
    print("Please specify a fractal configuration file!")
    print("Usage: main.py CONFIG.frac")
