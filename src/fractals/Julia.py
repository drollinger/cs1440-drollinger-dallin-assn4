# given coordinate in complex plane

# return the iteration count of function at points
from Fractal import Fractal


class Julia(Fractal):
    def __init__(self, iterations, z=complex(0, 0), c=complex(0, 0)):
        super().__init__(iterations, z, c)

    def iterationType(self, zStep):
        return zStep * zStep + self.c

    def setValueC(self, newValue):
        self.z = newValue
