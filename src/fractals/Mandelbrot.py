# given coordinate in complex plane
# return the iteration count of function at points
from Fractal import Fractal


class Mandelbrot(Fractal):
    def __init__(self, iterations, c=complex(0, 0)):
        super().__init__(iterations, c)

    def iterationType(self, zStep):
        return zStep * zStep + self.c