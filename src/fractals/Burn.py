# given coordinate in complex plane

# return the iteration count of function at points
from Fractal import Fractal


class Burn(Fractal):
    def __init__(self, iterations, c=complex(0, 0)):
        super().__init__(iterations, c)

    def iterationType(self, zStep):
        if (zStep * zStep + self.c) != 0:
            return 1 / (zStep * zStep - self.c) + self.c
        else:
            return zStep
