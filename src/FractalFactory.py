from fractals import Burn, DomePlay, Julia, Mandelbrot


def makeFractal(cfg):
    if cfg['type'] == 'mandelbrot':
        return Mandelbrot.Mandelbrot(cfg['iterations'])
    elif cfg['type'] == 'julia':
        return Julia.Julia(cfg['iterations'], complex(cfg['creal'], cfg['cimag']))
    elif cfg['type'] == 'burn':
        return Burn.Burn(cfg['iterations'])
    elif cfg['type'] == 'domeplay':
        return DomePlay.DomePlay(cfg['iterations'])
    else:
        raise NotImplementedError("Invalid fractal requested")
