from gradients import RandomColors, Rainbow, Grayscale, BlueGreen, DandyMan, America, Royal


def makeGradient(gradient, iterations):
    if gradient == 'rainbow':
        return Rainbow.Rainbow(iterations)
    elif gradient == 'grayscale':
        return Grayscale.Grayscale(iterations)
    elif gradient == 'bluegreen':
        return BlueGreen.BlueGreen(iterations)
    elif gradient == 'dandyman':
        return DandyMan.DandyMan(iterations)
    elif gradient == 'america':
        return America.America(iterations)
    elif gradient == 'royal':
        return Royal.Royal(iterations)
    else:
        return RandomColors.RandomColors(iterations)
