# Class idea from Erik Falor
from abc import ABC
import Color, math


class Gradient(ABC):
    def __init__(self, iterations):
        self.iterations = iterations

    def getGradientColorMap(self):
        colorMap = self.getColorList()
        gradientIterations = math.ceil(self.iterations / (len(colorMap) - 1))
        gradientColorMap = []
        for i in range(len(colorMap) - 1):
            gradientColorMap.extend(self.linerlyInterpolate(colorMap[i], colorMap[i+1], gradientIterations))
        gradientColorMap.append(gradientColorMap[-1])
        return gradientColorMap

    def getColorList(self):
        return []

    def linerlyInterpolate(self, start, stop, steps):
        dRed = (stop.r - start.r) / (steps - 1)
        dGrn = (stop.g - start.g) / (steps - 1)
        dBlu = (stop.b - start.b) / (steps - 1)
        return list(map(lambda n: Color.Color((n * dRed) + start.r, (n * dGrn) + start.g, (n * dBlu) + start.b), range(steps)))
