# reads in configuration file
import os
# generates configuration object(dic) to use in other parts


class Config:
    configDict = {}

    def __init__(self, fileInput):
        with open(fileInput) as file:
            for line in file:
                if ':' in line:
                    key, value = line.split(":")
                    if key.lower() == 'centerx':
                        centerX = float(value)
                    elif key.lower() == 'centery':
                        centerY = float(value)
                    else:
                        self.configDict[key.lower()] = value.strip().lower()
        self.configDict['pixels'] = int(self.configDict['pixels'])
        self.configDict['axislength'] = float(self.configDict['axislength'])
        self.configDict['min'] = {'x': centerX - (self.configDict['axislength'] / 2.0), 'y': centerY - (self.configDict['axislength'] / 2.0)}
        self.configDict['max'] = {'x': centerX + (self.configDict['axislength'] / 2.0), 'y': centerY + (self.configDict['axislength'] / 2.0)}
        self.configDict['pixelsize'] = abs(self.configDict['max']['x'] - self.configDict['min']['x']) / self.configDict["pixels"]
        self.configDict['iterations'] = int(self.configDict['iterations'])
        self.configDict['imageName'] = fileInput.split(".")[0]
        if self.configDict['type'] == 'julia':
            self.configDict['creal'] = float(self.configDict['creal'])
            self.configDict['cimag'] = float(self.configDict['cimag'])
