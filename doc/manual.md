# User Manuel
======================================

### How to use program from terminal
--------------------------------------

To run the program from the terminal...  
1. Make sure you are in the ../src directory in your terminal  
2. type the following --->   python main.py FILENAME.frac gradientType
   * note: gradient is optional and defaults to random colors
3. Instead of typing FILENAME.frac you will type the name of your configuration file  
  * _see below for details_  
4. Wait for image to load. There will be a status bar.  
5. Your image will pop up on your computer and an image will be saved in your ../data folder  

Note: images are currently saved as .png images  

gradientType options:
    America
    BlueGreen
    DandyMan
    Grayscale
    Rainbow
    Royal

### How to write a .frac file
--------------------------------------

To write a .frac file, follow the example given

For Mandelbrot:

	type: Mandelbrot
	pixels: 640
	centerX: 0.0
	centerY: 0.0
	axisLength: 4.0
	iterations: 100


For Julia:

	type: Julia
	cReal: -1
	cImag: 0
	pixels: 1024
	centerX: 0.0
	centerY: 0.0
	axisLength: 4.0
	iterations: 78

Note: There are also two other fractal types. Burn and DomePlay which follow the Mandelbrot layout

Note that the Julia .frac file includes a cReal and cImag that must be included for calculations

After writing .frac file, save as NAME.frac where NAME is chosen by you. 
